package com.binaredwin.samplecode.network

import com.binaredwin.samplecode.model.BaseResponsePokemon
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {
    @GET("pokemon")
    fun getPokemons() : Call<BaseResponsePokemon>
}