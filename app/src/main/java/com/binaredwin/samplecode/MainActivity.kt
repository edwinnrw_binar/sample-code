package com.binaredwin.samplecode

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.binaredwin.samplecode.databinding.ActivityMainBinding
import com.binaredwin.samplecode.model.BaseResponsePokemon
import com.binaredwin.samplecode.model.PokemonResult
import com.binaredwin.samplecode.network.NetworkClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

    lateinit var adapterPokemon: PokemonAdapter

    lateinit var pokemons:MutableList<PokemonResult>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        pokemons = mutableListOf()
        adapterPokemon = PokemonAdapter(pokemons)
        binding.rvPokemon.apply {
            layoutManager = GridLayoutManager(this@MainActivity,2)
            adapter = adapterPokemon
        }

        fetchPokemons()



    }

    private fun fetchPokemons(){
        NetworkClient.instance.getPokemons().enqueue(object : Callback<BaseResponsePokemon>{
            override fun onResponse(
                call: Call<BaseResponsePokemon>,
                response: Response<BaseResponsePokemon>
            ) {
                if (response.code() == 200){
                    val data = response.body()?.pokemonResults?: mutableListOf()
                    pokemons.addAll(data)
                    adapterPokemon.notifyDataSetChanged()
                }
            }

            override fun onFailure(call: Call<BaseResponsePokemon>, t: Throwable) {
                Toast.makeText(this@MainActivity,
                    "Error:"+t.localizedMessage,Toast.LENGTH_LONG).show()
            }

        })
    }
}