package com.binaredwin.samplecode

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.binaredwin.samplecode.databinding.ItemPokemonBinding
import com.binaredwin.samplecode.model.PokemonResult
import com.bumptech.glide.Glide

class PokemonAdapter(val pokemons:List<PokemonResult>) : RecyclerView.Adapter<PokemonViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pokemon,
            parent, false)
        return PokemonViewHolder(view)
    }

    override fun onBindViewHolder(holder: PokemonViewHolder, position: Int) {
        holder.bind(pokemons[position])
    }

    override fun getItemCount(): Int {
       return pokemons.size
    }
}


class PokemonViewHolder(itemView:View) : RecyclerView.ViewHolder(itemView){

    var binding: ItemPokemonBinding = ItemPokemonBinding.bind(itemView)

    fun bind(itemPokemon: PokemonResult){
        Glide.with(itemView)
            .load(getImageUrl(itemPokemon.url))
            .into(binding.ivPokemon)
        binding.tvPokemonName.text = itemPokemon.name
    }

    fun getImageUrl(url:String): String {
        val index = url.split("/".toRegex()).dropLast(1).last()
        return "https://pokeres.bastionbot.org/images/pokemon/$index.png"
    }
}