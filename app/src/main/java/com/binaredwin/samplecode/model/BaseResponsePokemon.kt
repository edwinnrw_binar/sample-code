package com.binaredwin.samplecode.model


import com.google.gson.annotations.SerializedName

data class BaseResponsePokemon(
    @SerializedName("next")
    val next: String,
    @SerializedName("previous")
    val previous: Any,
    @SerializedName("results")
    val pokemonResults: List<PokemonResult>
)